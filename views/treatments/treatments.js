angular.module('rokketMed.treatments', [])

    .controller('TreatmentsController', ["$scope", "$rootScope", "$http", "$log", "getData", "Angularytics" ,"$routeParams", function ($scope, $rootScope, $http, $log, getData, Angularytics, $routeParams) {
        'use strict';

        $rootScope.siteTitle = 'Treatments';
        $rootScope.bgImg = '';
        $scope.treatments = [];
        $scope.common = [];
        $scope.dataLoaded = false;
        var url = $routeParams.name;
        var store = {
            results: [],
            businessesGroups: []
        };

        $scope.gaTreatments = function (prop) {
            Angularytics.trackEvent('Treatment Category', 'treatment clicked', prop);
        };
        getData.getTreatmentsName()
            .then(function (data) {
                $scope.treatments = _.filter(data, function (item) {
                  /**************** begin ****************/
                    /*
                    item.url = encodeURIComponent(item.name);
                    getData.getTreatments(item.name).then(function(data){
                      store.results = data[0];
                      var tempBusinesses = data[1];
                      var tempResultData;

                      $log.log('Data ' + data[1]);
                      for (var business in tempBusinesses) {

                          tempResultData = tempBusinesses[business].treatmentData;

                          for (var result in tempResultData) {



                              if (tempResultData[result].id === store.results._id && tempResultData[result].published === true) {
                                  // tempBusinesses[business].address = formAddress(tempBusinesses[business].addr1, tempBusinesses[business].city, tempBusinesses[business].state, tempBusinesses[business].zip);

                                  if(typeof tempResultData[result].providerPrice === 'string' && tempResultData[result].providerPrice.indexOf('-') !== -1){

                                      var price = tempResultData[result].providerPrice.split('-');

                                      if(parseInt(price[0]) < parseInt($scope.minPrice) || $scope.minPrice === 0){
                                          $scope.minPrice = price[0];
                                      }
                                      if(parseInt(price[1]) > parseInt($scope.maxPrice)){
                                          $scope.maxPrice = price[1];
                                      }

                                      // $log.log('minPrice ' +$scope.minPrice);
                                      // $log.log('minPrice ' +$scope.maxPrice);
                                  }else{
                                      if(parseInt(tempResultData[result].providerPrice) < parseInt($scope.minPrice) || $scope.minPrice === 0){
                                          $scope.minPrice = tempResultData[result].providerPrice;
                                      }
                                      if(parseInt(tempResultData[result].providerPrice) > parseInt($scope.maxPrice)){
                                          $scope.maxPrice = tempResultData[result].providerPrice;
                                      }
                                  }

                              }
                          }
                      }
                    });
                    */
                    /**************** end ****************/

                    if (typeof item.businessesCount != 'undefined') {
                        return !!item.businessesCount;
                    }
                    return true;
                });
                $scope.common = _.filter(data, function (item) {
                    item.url = encodeURIComponent(item.name);
                    return item.description2 == "Common";
                });

                $scope.dataElements = $scope.treatments;
            })
            .then(function () {
                $scope.dataLoaded = true;
            })
            .catch($log.err);

    }]);
